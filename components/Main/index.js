/** @jsx h */

import { component, h } from 'literaljs';
import './index.scss';

export default component({
	state: {
		repoURL: 'https://gitlab.com/Mikeysax/LiteralJS',
		websiteURL: 'https://literaljs.com'
	},
	render() {
		const { repoURL, websiteURL } = this.getState();
		return (
			<div class="Main">
				<div class="logo">
					<div class="L">L</div>
					<div class="J">J</div>
					<div class="S">S</div>
				</div>
				<h1>
					<span class="Literal">Literal</span>
					<span class="JS">JS</span>
				</h1>
				<h2>Welcome To The LiteralJS Starter Application!</h2>
				<a href={repoURL} target="_blank" rel="noopener noreferrer">
					Gitlab
				</a>
				<a href={websiteURL} target="_blank" rel="noopener noreferrer">
					Documentation
				</a>
			</div>
		);
	}
});
